package com.java.sensors.eventTool;

import com.sensorsdata.analytics.javasdk.SensorsAnalytics;
import com.sensorsdata.analytics.javasdk.exceptions.InvalidArgumentException;

import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SendEvent {
  static final String HOST_SAAS = "http://10.129.140.236:8106";
  static final String HOST_PAAS = "http://10.129.140.236:8106";
  static final String IDFA = "399F9C00-92DC-4B5C-9464-7971F01F837D";
  static final String SA_SERVER_URL = HOST_PAAS+"/sa?project=production";

  public static String simple_ios_install_source =
      "imei=862813038213735##oaid=cab14297cac9d346##idfa=599F9C00-92DC-4B5C-9464-7971F01F837A##idfv=599F9C00-92DC-4B5C-9464-7971F01F837V";
  static final RestTemplate restTemplate = new RestTemplate();

  public static void main(String[] args) throws IOException, InvalidArgumentException {
    SensorsAnalytics sa = new SensorsAnalytics(
        new SensorsAnalytics.BatchConsumer(SA_SERVER_URL, 1, 0, true));

    try {
      $AppChannelMatchingHttp(sa);
      Thread.sleep(1000);//点击激活错开30s

      $AppInstall(sa);
      System.out.println("done...");
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      sa.shutdown();
    }
  }

  static void $AppChannelMatchingHttp(SensorsAnalytics sa) throws InvalidArgumentException {
    String clickUrl = HOST_SAAS+"/cb/installation_track?_channel_app_id=no&_channel_track_key=eM8HMnPY&project=production&channel_name=toutiao_track&channel_ad_name=__CID_NAME__&os=__OS__&channel_campaign_name=__CAMPAIGN_NAME__&idfa=__IDFA__&ip=__IP__&ua=__UA__&channel_account_id=__ADVERTISER_ID__&mac=__MAC__&channel_campaign_id=__CAMPAIGN_ID__&callback_url=__CALLBACK_URL__&channel_click_id=__REQUEST_ID__&channel_ad_id=__CID__&click_time=__TS__&caid1=__CAID1__&caid2=__CAID2__&model=__MODEL__&channel_adgroup_name=__AID_NAME__&channel_adgroup_id=__AID__&channel_link_type=app";
    clickUrl = clickUrl.replaceAll("__IDFA__", IDFA);
    StringBuilder stringBuilder = new StringBuilder(clickUrl);
    stringBuilder.append("&utm_source=").append("wangff1103");
    restTemplate.getForEntity(stringBuilder.toString(),String.class);
  }

  static void $AppInstall(SensorsAnalytics sa) throws InvalidArgumentException {
    String distinctId = UUID.randomUUID().toString();
    Map<String, Object> properties = new HashMap();
//    "$ios_install_source": "android_id=4f295025f34c171a##imei=##mac=B0:55:08:28:14:7C##gaid=e4fe9bde-caa0-47b6-908d-ffba3fa184f2"
    String $ios_install_source = "android_id=4f295025f34c171a##imei=##mac=B0:55:08:28:14:7C##idfa=" + IDFA;
    properties.put("$app_id", "no");
    properties.put("$time", asDate(LocalDateTime.now())); // 这条event发生的时间，如果不设置的话，则默认是当前时间
//    properties.put("$os", "22"); // 通过请求中的UA，可以解析出用户使用设备的操作系统是windows的
    properties.put("$os_version","8.1"); // 操作系统的具体版本
    properties.put("$ip", "127.0.0.1"); // 请求中能够拿到用户的IP，则把这个传递给SA，SA会自动根据这个解析省份、城市
    properties.put("Channel", "toutiao"); // 用户是通过baidu这个渠道过来的
    properties.put("$ios_install_source", $ios_install_source);
    sa.track(distinctId, false, "$AppInstall", properties);
  }

  //LocalDateTime -> Date
  public static Date asDate(LocalDateTime localDateTime) {
    return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
  }
  static void $ChannelLinkReaching(SensorsAnalytics sa) throws InvalidArgumentException {
    String cookieId = "wangff_3";
    String a = "https://satxyhtest.debugbox.sensorsdata.cn/sat/AppInstall.html?testhehe=12324&utm_source=sina&_track_key=AlhomVJP&%24sd_import_keys=testhehe&project=default&name=360_sem_track&sat_cf=1&link_type=web";
    Map<String, Object> properties = new HashMap();
    properties.clear();
    properties.put("$lib", "js");
    properties.put("_sa_channel_landing_url",a );
    sa.track(cookieId, false, "$ChannelLinkReaching", properties);
  }



}
