package com.java.sensors.eventTool;

import com.sensorsdata.analytics.javasdk.SensorsAnalytics;
import com.sensorsdata.analytics.javasdk.exceptions.InvalidArgumentException;

import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AddEvent {
 static final String SA_SERVER_URL = "http://10.120.219.0:8106/sa?project=default";
// static final String SA_SERVER_URL = "http://10.129.140.236:8106/sa?project=production";
//  static final String SA_SERVER_URL = "http://10.120.53.62:8106/sa?project=production";
  static final RestTemplate restTemplate = new RestTemplate();

  public static void main(String[] args) throws IOException, InvalidArgumentException {
    WangffSensorsAnalytics sa = new WangffSensorsAnalytics(new WangffSensorsAnalytics.BatchConsumer(SA_SERVER_URL, 1, 0, true));
    try {
//      sa.setEnableTimeFree(true);
//      $AppChannelMatchingHttp(sa);
//      $AppChannelMatching(sa);
//      $AppInstall(sa);
      $ChannelLinkReaching(sa);

      sa.flush();
      System.out.println("done...");
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      sa.shutdown();
    }
  }

  static void $AppChannelMatchingHttp(SensorsAnalytics sa) throws InvalidArgumentException {
    StringBuilder url = new StringBuilder("http://10.129.140.236:8106/cb/installation_track?_channel_app_id=no&_channel_track_key=PbtsQU68&project=default&channel_name=toutiao_track&os=__OS__&idfa=e4fe9bde-caa0-47b6-908d-ffba3fa184f2&ip=__IP__&ua=__UA__&channel_account_id=__ADVERTISER_ID__&mac=__MAC__&channel_campaign_id=__CAMPAIGN_ID__&callback_url=__CALLBACK_URL__&channel_click_id=__REQUEST_ID__&channel_ad_id=__CID__&caid1=__CAID1__&caid2=__CAID2__&model=__MODEL__&channel_adgroup_id=__AID__");
    url.append("&utm_source=").append(Math.random());
    restTemplate.getForEntity(url.toString(),String.class);
  }

  static void $AppChannelMatching(WangffSensorsAnalytics sa) throws InvalidArgumentException {
    String distinct_id = "custom_channel_track_fake_id";
    Map<String, Object> properties = new HashMap();
    properties.clear();
    properties.put("_channel_track_key", "55555555");
    properties.put("$utm_source", "1111");
    properties.put("channel_name", "toutiao_track");
    properties.put("idfa", "e4fe9bde-caa0-47b6-908d-ffba3fa184f9");
    sa.track_installation(distinct_id, false, "$AppChannelMatching", properties);
  }


  static void $AppInstall(SensorsAnalytics sa) throws InvalidArgumentException {
    String cookieId = "wangff";
    Map<String, Object> properties = new HashMap();
    properties.clear();
    properties.put("$time", new Date()); // 这条event发生的时间，如果不设置的话，则默认是当前时间
//    properties.put("$os", params[1]); // 通过请求中的UA，可以解析出用户使用设备的操作系统是windows的
    properties.put("$os_version","8"); // 操作系统的具体版本
//    properties.put("$ip", params[3]); // 请求中能够拿到用户的IP，则把这个传递给SA，SA会自动根据这个解析省份、城市
    properties.put("Channel", "toutiao"); // 用户是通过baidu这个渠道过来的
    properties.put("$app_d", "com.wangff"+ Math.random());
    //"$ios_install_source": "android_id=4f295025f34c171a##imei=##mac=B0:55:08:28:14:7C##gaid=e4fe9bde-caa0-47b6-908d-ffba3fa184f2"
    properties.put("$ios_install_source", "android_id=4f295025f34c171a##imei=##mac=B0:55:08:28:14:7C##gaid=e4fe9bde-caa0-47b6-908d-ffba3fa184f9##idfa=599F9C00-92DC-4B5C-9464-7971F01F837A");
    sa.track(cookieId, false, "$AppInstall", properties);
  }

  static void $ChannelLinkReaching(SensorsAnalytics sa) throws InvalidArgumentException {
    String distinctId = "wangff_"+System.currentTimeMillis();
    String _sa_channel_landing_url = "https://satxyhtest.debugbox.sensorsdata.cn/sat/AppInstall.html?testhehe=12324&utm_source=sina&_track_key=AlhomVJP&%24sd_import_keys=testhehe&project=default&name=360_sem_track&sat_cf=2&link_type=web";
    Map<String, Object> properties = new HashMap();
    properties.clear();
    properties.put("$lib", "js");
    properties.put("_sa_channel_landing_url",_sa_channel_landing_url );
    sa.track(distinctId, false, "$ChannelLinkReaching", properties);
  }



}
