package com.java.sensors.Interface.service;


import com.java.sensors.Interface.duotai.Daughter;

/**
 * TODO
 *
 * @author zhanghongqiang
 * @version 1.0.0
 * @since 2022/01/28 19:44
 */
public class ZhaoShangBankImpl extends AbstractBank<Daughter> {
  private Daughter daughter;
  @Override
  protected void initOSSClient(Daughter globalConfig) {
    daughter = globalConfig;
  }

  @Override
  protected void doGetConfig() {
    System.out.println(daughter.getFatherName());
    System.out.println(daughter.getDaughterName());
  }
}
