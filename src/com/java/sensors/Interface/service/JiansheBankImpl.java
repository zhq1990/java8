package com.java.sensors.Interface.service;


import com.java.sensors.Interface.duotai.Son;

/**
 * TODO
 *
 * @author zhanghongqiang
 * @version 1.0.0
 * @since 2022/01/28 19:44
 */
public class JiansheBankImpl extends AbstractBank<Son> {
  private Son son;

  @Override
  protected void initOSSClient(Son globalConfig) {
    son = globalConfig;
  }

  @Override
  protected void doGetConfig() {
    System.out.println(son.getFatherName());
    System.out.println(son.getSonName());
  }
}
