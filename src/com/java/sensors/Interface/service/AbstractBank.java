package com.java.sensors.Interface.service;


import com.java.sensors.Interface.duotai.Father;

/**
 * TODO
 *
 * @author zhanghongqiang
 * @version 1.0.0
 * @since 2022/01/28 19:43
 */
public abstract class AbstractBank<T extends Father> implements Bank<T> {

  @Override
  public void init(T globalConfig) {
    initOSSClient(globalConfig);
  }

  protected abstract void initOSSClient(T globalConfig);

  @Override
  public void getConfig() {
    doGetConfig();
  }

  protected abstract void doGetConfig();


  @Override
  public void close() throws Exception {

  }
}
