package com.java.sensors.Interface.service;


import com.java.sensors.Interface.duotai.Father;

/**
 * TODO
 *
 * @author zhanghongqiang
 * @version 1.0.0
 * @since 2022/01/28 19:42
 */
public interface Bank<T extends Father> extends AutoCloseable {

  void init(T globalConfig);

  void  getConfig();

}
