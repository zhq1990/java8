package com.java.sensors.Interface;


import com.java.sensors.Interface.duotai.Daughter;
import com.java.sensors.Interface.duotai.Father;
import com.java.sensors.Interface.duotai.Son;
import com.java.sensors.Interface.service.Bank;
import com.java.sensors.Interface.service.JiansheBankImpl;
import com.java.sensors.Interface.service.ZhaoShangBankImpl;

/**
 * TODO
 *
 * @author zhanghongqiang
 * @version 1.0.0
 * @since 2022/01/28 19:26
 */
public class Test {
  public static void main(String[] args) {
    Father father;

    Son son = new Son();
    son.setFatherName("张小强");
    son.setSonName("张闹闹");
    father = son;

    Bank oss = new JiansheBankImpl();
    oss.init(father);
    oss.getConfig();

    Daughter daughter = new Daughter();
    daughter.setFatherName("张小强");
    daughter.setDaughterName("张橙子");
    father = daughter;

    Bank oss1 = new ZhaoShangBankImpl();
    oss1.init(father);
    oss1.getConfig();


  }
}
