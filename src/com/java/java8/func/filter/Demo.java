package com.java.java8.func.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * https://segmentfault.com/a/1190000020158145
 */
public class Demo {
	public static void main(String[] args) {
		java7();
		java8();
	}

	/**
	 * 1. Streams filter() and collect()
	 */
	public static void java7() {
		List<String> lines = Arrays.asList("spring", "node", "php");

		List<String> result = getFilterOutput(lines, "php");
		for (String temp : result) {
			System.out.println(temp);    //output : spring, node
		}
	}

	private static List<String> getFilterOutput(List<String> lines, String filter) {
		List<String> result = new ArrayList<>();
		for (String line : lines) {
			if (!"php".equals(line)) { // we dont like php
				result.add(line);
			}
		}
		return result;
	}


	public static void java8() {
		List<String> lines = Arrays.asList("spring", "node", "php");

		List<String> result = lines.stream()          // convert list to stream
				.filter(line -> !"php".equals(line))      // we dont like php
				.collect(Collectors.toList());            // collect the output and convert streams to a List

		result.forEach(System.out::println);          //output : spring, node
	}

}
