package com.java.java8.func.filter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * https://segmentfault.com/a/1190000020158145
 */
public class Demo1 {

	/**
	 * 2. Streams filter(), findAny() and orElse()
	 */
	public static void main(String[] args) {
		test1();
		test2();
		test3();
		test4();
	}
	//java8 ֮��
	public static void test1() {
		System.out.println("��ʼִ��test1>>>>>>>>>>>>");
		List<Developer> persons = Arrays.asList(
				new Developer("zhangsan", 20),
				new Developer("lisi",21),
				new Developer("wangwu",22));

		Developer developer = getByNameBefore(persons,"lisi");
		System.out.println(developer == null ? "������" : developer.toString());
	}

	private static Developer getByNameBefore(List<Developer> persons, String lisi) {
		Developer result = null;
		for (Developer developer : persons) {
			if (developer.getName().equals(lisi)) {
				result = developer;
			}
		}

		return result;
	}

	//java8 ֮��
	public static void test2() {
		System.out.println("��ʼִ��test2>>>>>>>>>>>>");
		List<Developer> persons = Arrays.asList(
				new Developer("zhangsan", 20),
				new Developer("lisi",21),
				new Developer("wangwu",22));

		Developer developer = persons.stream().filter(x -> "lisi".equals(x.getName())).findAny()
				.orElse(null);
		System.out.println(developer);

		// �����ڵ�
		Developer developer1 = persons.stream().filter(x -> "asan".equals(x.getName())).findAny()
				.orElse(null);
		System.out.println(developer1);
	}

	//��������ѯʹ�ã�
	public static void test3() {
		System.out.println("��ʼִ��test3>>>>>>>>>>>>");
		List<Developer> persons = Arrays.asList(
				new Developer("zhangsan", 20),
				new Developer("lisi",21),
				new Developer("wangwu",22));

		Developer result1 = persons.stream()
				.filter(p -> "lisi".equals(p.getName()) && p.getAge() == 21).findAny().orElse(null);

		System.out.println(result1);

		Developer result2 = persons.stream().filter(p -> {
			if ("lisi".equals(p.getName()) && p.getAge() == 21) {
				return true;
			}
			return false;
		}).findAny().orElse(null);

		System.out.println(result2);
	}

	/**
	 * 3. Streams filter() and map()
	 */
	public static void test4() {
		System.out.println("��ʼִ��test4>>>>>>>>>>>>");
		List<Developer> persons = Arrays.asList(
				new Developer("zhangsan", 20),
				new Developer("lisi",21),
				new Developer("zhaoliu",21),
				new Developer("wangwu",22));

		String name = persons.stream().filter(x -> "lisi".equals(x.getName()))
				.map(Developer::getName)     //convert stream to String
				.findAny().orElse("");

		System.out.println("name : " + name);	

		List<String> collect2 = persons.stream().filter(x -> 21 == x.getAge())
				.map(Developer::getName)     //convert stream to String
				.collect(Collectors.toList());

		collect2.forEach(System.out::println);

		List<String> collect = persons.stream().filter(x -> "lisi".equals(x.getName()))
				.map(Developer::getName).collect(Collectors.toList());

		collect.forEach(System.out::println);

	}

}

class Developer {
	private String name;
	private int age;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Developer(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}
	@Override
	public String toString() {
		return "Developer [name=" + name + ", age=" + age + "]";
	}

}

