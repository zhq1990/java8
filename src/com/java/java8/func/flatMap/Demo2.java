package com.java.java8.func.flatMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.alibaba.fastjson.JSONObject;

public class Demo2 {

	public static void main(String[] args) {
		testMapAndFlatMap();
		//test1();
	}
	
	public static void testMapAndFlatMap() {  
        List<String> words = new ArrayList<String>();  
        words.add("hello");  
        words.add("word");  
  
        //��words�����е�Ԫ���ٰ����ַ���֣�Ȼ���ַ�ȥ�أ����մﵽ["h", "e", "l", "o", "w", "r", "d"]  
        //���ʹ��map���Ǵﲻ��ֱ��ת����List<String>�Ľ��  
        List<String> stringList = words.stream()  
                .flatMap(word -> Arrays.stream(word.split("")))  
                .distinct()  
                .collect(Collectors.toList());  
        stringList.forEach(e -> System.out.println(e));  
    }

	public static void test1() {
		List<String> words = new ArrayList<String>();
		words.add("your");
		words.add("name");

		Stream<Stream<Character>> result = words.stream().map(w -> characterStream(w));  
		Stream<Character> letters = words.stream().flatMap(w -> characterStream(w)); 
	}

	public static Stream<Character> characterStream(String s){  
		List<Character> result = new ArrayList<>();  
		for (char c : s.toCharArray()) 
			result.add(c);
		return result.stream();  
	}


}
