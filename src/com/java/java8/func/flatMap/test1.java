package com.java.java8.func.flatMap;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class test1 {
	
	public static void main(String[] args) {
		String[] words = new String[]{"Hello","World"};
	    List<String[]> collect = Arrays.stream(words)
	            .map(word -> word.split(""))
	            .distinct()
	            .collect(Collectors.toList());
	}
    
}
