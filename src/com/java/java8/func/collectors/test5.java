
package com.java.java8.func.collectors;

import java.util.ArrayList;
import java.util.List;

public class test5 {

	public static void main(String[] args) {

		List<Integer> li = new ArrayList<>();
		li.add(1);
		//li.add(2);
		//li.add(3);
		Integer re = li.stream().reduce((x, y) -> x + y).get();
		System.out.println(re);
		
	//	System.out.println(Stream.of(1, 2, 3, 4).reduce(100, (sum, item) -> sum + item));
		
	}

}
