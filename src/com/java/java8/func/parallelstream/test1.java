package com.java.java8.func.parallelstream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class test1 {

	public static void main(String[] args) throws InterruptedException  
	{  
		Integer[] intArray = {1, 2, 3, 4, 5, 6, 7, 8};  
		List<Integer> listOfIntegers =  
				new ArrayList<>(Arrays.asList(intArray));  
		List<Integer> parallelStorage = new ArrayList<>();//Collections.synchronizedList(new ArrayList<>());  
		listOfIntegers  
		.parallelStream()  
		// Don't do this! It uses a stateful lambda expression.  
		.map(e -> {  
			parallelStorage.add(e);  
			return e;  
		})  
		.forEachOrdered(e -> System.out.print(e + " "));  
		System.out.println();  
		parallelStorage  
		.stream()  
		.forEachOrdered(e -> System.out.print(e + " "));  
		System.out.println();  
		System.out.println("Sleep 5 sec");  
		TimeUnit.SECONDS.sleep(5);  
		parallelStorage  
		.stream()  
		.forEachOrdered(e -> System.out.print(e + " "));  
	}  
	
	/*Result 1:  
		1 2 3 4 5 6 7 8   
		null 3 8 7 1 4 5 6   
		Sleep 5 sec  
		null 3 8 7 1 4 5 6  
		  
		Result 2：  
		1 2 3 4 5 6 7 8   
		6 2 4 1 5 7 8   
		Sleep 5 sec  
		6 2 4 1 5 7 8 */

}
