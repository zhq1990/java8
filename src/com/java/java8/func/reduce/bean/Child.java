package com.java.java8.func.reduce.bean;

public class Child extends Foo{

	public Child() {
		
	}
	
	private String name="子类";
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void insert() {
		System.out.println("getName():      "+getName());
		System.out.println("this.getName(): "+this.getName());
		System.out.println("super.getName():"+super.getName());
	}
	
	public static void main(String[] args) {
		Child child=new Child();
		child.insert();
	}

}
