package com.java.java8.lambda.lambda;

import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.BinaryOperator;

/**
 * 3.lambda�ܽ�#
	���沢û�и���̫���lambdaʵ����ֻ�ǲ��ؽ������ȥ���lambda���ʽ���������Ҫ�¡�
	
	Ҫ��סlambda�ı��ʣ�Ϊ�����ͽӿڵ�����ʵ�ֽ��м�����򻯡�
	
	��ν�ļ򻯾���lambda�ı�׼��ʽ����ν�ĸ������ڱ�׼��ʽ�Ļ����Ͻ��з������ú͹������á�
	
	���������������еķ���ȥʵ�ִ˿̵Ľӿڡ�
	
	���������ǶԷ�����ֻ��һ��new Object()�Ľ�һ���򻯡�
 */

public class Lambda {
	/*
	 * https://github.com/CarpenterLee/JavaLambdaInternals/blob/master/1-Lambda%20and%20Anonymous%20Classes(I).md
	 * https://www.cnblogs.com/yueshutong/p/9735157.html
	 * ��д�����ݣ�
		 * �ܹ�ʹ��Lambda�������Ǳ�������Ӧ�ĺ����ӿڣ������ӿڣ���ָ�ڲ�ֻ��һ�����󷽷��Ľӿڣ���
		 * ��һ���Java��ǿ���������Ǻϣ�Ҳ����˵�㲢�����ڴ�����κεط����Ե�дLambda���ʽ��
		 * ʵ����Lambda�����;��Ƕ�Ӧ�����ӿڵ����͡�
		 * Lambda���ʽ��һ�������������ƶϻ��ƣ�����������Ϣ�㹻������£������������ƶϳ�����������ͣ�������Ҫ��ʽָ����
	 */
	private void Demos() {
		/*
		 *1չʾ���޲κ����ļ�д��
		 *2��չʾ���вκ����ļ�д���Լ������ƶϻ��ƣ�
		 *3�Ǵ�����д����
		 *4��5�ٴ�չʾ�������ƶϻ��ơ�
		 */
		// Lambda���ʽ����д��ʽ
		Runnable run = () -> System.out.println("Hello World");// 1
		ActionListener listener = event -> System.out.println("button clicked");// 2
		Runnable multiLine = () -> {// 3 �����
		    System.out.print("Hello");
		    System.out.println(" Hoolee");
		};
		BinaryOperator<Long> add = (Long x, Long y) -> x + y;// 4
		BinaryOperator<Long> addImplicit = (x, y) -> x + y;// 5 �����ƶ�
	}
	
	public static void main(String[] args) {
		//�޲κ����ļ�д
		jdk7InnerClass();		
		jdk8Lambda();
	}
	
	//�޲κ����ļ�д
	public static void jdk7InnerClass() {
		System.out.println("jdk7InnerClass()>>>>>>>>>  ");
		// JDK7 �����ڲ���д��
		new Thread(new Runnable(){// �ӿ���
			@Override
			public void run(){// ������
				System.out.println("Thread run()");
			}
		}).start();
	}
	
	public static void jdk8Lambda() {
		System.out.println("jdk8Lambda()>>>>>>>>>  ");
		// JDK8 Lambda���ʽд��
		new Thread(
				() -> System.out.println("Thread run()")// ʡ�Խӿ����ͷ�����
		).start();
	}
	public static void jdk8Lambda2() {
		System.out.println("jdk8Lambda()>>>>>>>>>  ");
		// JDK8 Lambda���ʽ�����д��
		new Thread(
		        () -> {
		            System.out.print("Hello");
		            System.out.println(" Hoolee");
		        }
		).start();
		
		
	}
	
	
	public static void jdk7InnerClassWithParam() {
		System.out.println("jdk7InnerClass()>>>>>>>>>  ");
		// JDK7 �����ڲ���д��
		List<String> list = Arrays.asList("I", "love", "you", "too");
		Collections.sort(list, new Comparator<String>(){// �ӿ���
		    @Override
		    public int compare(String s1, String s2){// ������
		        if(s1 == null)
		            return -1;
		        if(s2 == null)
		            return 1;
		        return s1.length()-s2.length();
		    }
		});
		list.forEach((d -> System.out.println(d)));
		
	}
	
	public static void jdk8LambdaWithParam() {
		System.out.println("jdk8Lambda()>>>>>>>>>  ");
		// JDK8 Lambda���ʽд��
		List<String> list = Arrays.asList("I", "love", "you", "too");
		Collections.sort(list, (s1,s2) -> { // ʡ�Բ����������
			if(s1 == null)
		        return -1;
		    if(s2 == null)
		        return 1;
		    return s1.length()-s2.length();
		});
		list.forEach(System.out::println);
	}
	
}
