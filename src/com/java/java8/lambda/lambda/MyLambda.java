package com.java.java8.lambda.lambda;

import java.util.Arrays;
import java.util.List;

public class MyLambda {
	//�Զ��庯���ӿ�
	//https://github.com/CarpenterLee/JavaLambdaInternals/blob/master/1-Lambda%20and%20Anonymous%20Classes(I).md
	public static void main(String[] args) {
		lambda();
		myStreamDemo();
	}
	public static void lambda() {
		System.out.println("lambda()>>>>>>>>>  ");
		// TODO Auto-generated constructor stub
		ConsumerInterface<String> consumer = new ConsumerInterface<String>() {
			
			@Override
			public void accept(String t) {
				// TODO Auto-generated method stub
			}
		};
		
		ConsumerInterface<String> consumer1 = str -> System.out.println(str);
		System.out.println(consumer);
		System.out.println(consumer1);
	}
	
	public static void myStreamDemo() {
		System.out.println("myStreamDemo()>>>>>>>>>  ");
		//Stream<T>
		List<String> list = Arrays.asList("I", "love", "you", "too");
		MyStream<String> stream = new MyStream<String>();
		stream.list=list;
		stream.myForEach(str -> System.out.println(str));// ʹ���Զ��庯���ӿ���дLambda���ʽ
		System.out.println(stream);
	}
}
