package com.java.java8.lambda.demo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/*
 * ��2��ʹ��Java 8 lambda���ʽ�����¼�����
 * ������ù�Swing API��̣���ͻ�ǵ�����д�¼��������롣������һ���ɰ汾��������ľ��������������ڿ��Բ������ˡ��������lambda���ʽд�����õ��¼��������룬������ʾ��
 * Java�����߾���ʹ�����������һ���ط���Ϊ Collections.sort() ���� Comparator����Java 8�У�������ø��ɶ���lambda���ʽ������ª�������ࡣ�Ұ����������ϰ��Ӧ�ò��ѣ����԰�������ʹ��lambda���ʽʵ�� Runnable �� ActionListener �Ĺ����е���·������
 */
import javax.swing.JButton;

public class Demo2 {
	public static void main(String[] args) {
		// Java 8֮ǰ��
		JButton show =  new JButton("Show");
		show.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Event handling without lambda expression is boring");
			}
		});

		// Java 8��ʽ��
		show.addActionListener((e) -> {
			System.out.println("Light, Camera, Action !! Lambda expressions Rocks");
		});
	}

}
