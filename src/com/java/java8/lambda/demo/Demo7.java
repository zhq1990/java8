package com.java.java8.lambda.demo;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Demo7 {
	public static void main(String[] args) {
		List<String> strList = Arrays.asList("Lambdas", "Default Method", "Stream API", "Date and Time API");
		// ����һ���ַ����б�ÿ���ַ������ȴ���2
		List<String> filtered = strList.stream().filter(x -> x.length()> 2).collect(Collectors.toList());
		System.out.printf("Original List : %s, filtered list : %s %n", strList, filtered);
	}

}
