package com.java.java8.lambda.demo;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/*
 * ��5�������lambda���ʽ�м���Predicate
 * �ϸ�����˵����java.util.function.Predicate �������������� Predicate �ϳ�һ�������ṩ�������߼�������AND��OR�ķ��������ֽ���and()��or()��xor()�����ڽ����� filter() �����������ϲ����������磬Ҫ�õ�������J��ʼ������Ϊ�ĸ���ĸ�����ԣ����Զ������������� Predicate ʾ���ֱ��ʾÿһ��������Ȼ���� Predicate.and() ���������Ǻϲ�������������ʾ��
 */
public class Demo5 {
	public static void main(String[] args) {
		List<String> features = Arrays.asList("Lambdas", "Default Method", "Stream API", "Date and Time API");
		// ����������and()��or()��xor()�߼��������ϲ�Predicate��
		// ����Ҫ�ҵ�������J��ʼ������Ϊ�ĸ���ĸ�����֣�����Ժϲ�����Predicate������
		Predicate<String> startsWithJ = (n) -> n.startsWith("J");
		Predicate<String> fourLetterLong = (n) -> n.length() == 4;
		features.stream()
		    .filter(startsWithJ.and(fourLetterLong))
		    .forEach((n) -> System.out.print("nName, which starts with 'J' and four letter long is : " + n));
	}
}
