package com.java.java8.lambda.demo;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/*
 * ��4��ʹ��lambda���ʽ�ͺ���ʽ�ӿ�Predicate
	���������Բ���֧�ֺ���ʽ��̷��Java 8Ҳ�����һ���������� java.util.function���������˺ܶ��࣬����֧��Java�ĺ���ʽ��̡�����һ������Predicate��ʹ�� java.util.function.Predicate ����ʽ�ӿ��Լ�lambda���ʽ��������API��������߼����ø��ٵĴ���֧�ָ���Ķ�̬��Ϊ��������Java 8 Predicate �����ӣ�չʾ�˹��˼������ݵĶ��ֳ��÷�����Predicate�ӿڷǳ������������ˡ�
 */
public class Demo4 {
	public static void main(String[] args) {
	    List<String> languages = Arrays.asList("Java", "Scala", "C++", "Haskell", "Lisp");
	 
	    System.out.println("Languages which starts with J :");
	    filter(languages, (str)->((String) str).startsWith("J"));
	 
	    System.out.println("Languages which ends with a ");
	    filter(languages, (str)->((String) str).endsWith("a"));
	 
	    System.out.println("Print all languages :");
	    filter(languages, (str)->true);
	 
	    System.out.println("Print no language : ");
	    filter(languages, (str)->false);
	 
	    System.out.println("Print language whose length greater than 4:");
	    filter(languages, (str)->((String) str).length() > 4);
	}
	 
/*	public static void filter(List<String> names, Predicate condition) {
	    for(String name: names)  {
	        if(condition.test(name)) {
	            System.out.println(name + " ");
	        }
	    }
	}*/
	
	// ���õİ취
	public static void filter(List<String> names, Predicate condition) {
	    names.stream().filter((name) -> (condition.test(name))).forEach((name) -> {
	        System.out.println(name + " ");
	    });
	}

}
