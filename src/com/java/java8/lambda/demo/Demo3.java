package com.java.java8.lambda.demo;

import java.util.Arrays;
import java.util.List;

/*
 * ��3��ʹ��lambda���ʽ���б���е���
 * �����ʹ������Java�����֪����Լ����࣬����Ĳ������ǽ��е���������ҵ���߼�Ӧ���ڸ���Ԫ�أ����紦���������׺��¼����б�����Java������ʽ���ԣ�Java 8֮ǰ������ѭ�����붼��˳��ģ������Զ���Ԫ�ؽ��в��л�����������������й��ˣ�����Ҫ�Լ�д���룬�Ⲣ������ô���ס�ͨ������lambda���ʽ��Ĭ�Ϸ���������ʲô����ô��������ֿ��ˣ�����ζ��Java��������֪����������������������API����Լ���Ԫ�ؽ��в��д��������������ҽ����������ʹ��lambda��ʹ��lambda���ʽ������µ����б�����Կ����б���������һ�� forEach()  �����������Ե������ж��󣬲������lambda����Ӧ�������С�
 * �б�ѭ�������һ������չʾ�������Java 8��ʹ�÷������ã�method reference��������Կ���C++�����˫ð�š���Χ����������������Java 8��������ʾ�������á�
 */
public class Demo3 {
	public static void main(String[] args) {
		// Java 8֮ǰ��
		List<String> features = Arrays.asList("Lambdas", "Default Method", "Stream API", "Date and Time API");
		for (String feature : features) {
		    System.out.println(feature);
		}

		// Java 8֮��
		List features1 = Arrays.asList("Lambdas", "Default Method", "Stream API", "Date and Time API");
		features1.forEach(n -> System.out.println(n));
		 
		// ʹ��Java 8�ķ������ø����㣬����������::˫ð�Ų�������ʾ��
		// ��������C++����������������
		features1.forEach(System.out::println);
	}

}
