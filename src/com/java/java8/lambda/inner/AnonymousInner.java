package com.java.java8.lambda.inner;

/*
 * https://blog.csdn.net/chengqiuming/article/details/91352913
 * 1 �㾦
	��ͨ���ӿ������������ڲ���ʱ�������ڲ��಻����ʾ��������������������ڲ�����ֻ��һ����ʽ���޲ι���������new�ӿ�����������ﲻ�ܴ������ֵ��
	���ͨ���̳и��������������ڲ���ʱ�������ڲ��ཫӵ�к͸������ƵĹ��������˴�������ָ����ӵ����ͬ���β��б�
 */
public class AnonymousInner
{
	public void test(Device d)
	{
		System.out.println("������һ��" + d.getName()
		+ "��������" + d.getPrice());
	}
	public static void main(String[] args)
	{
		AnonymousInner ai1 = new AnonymousInner();
		// �����޲����Ĺ���������Device����ʵ����Ķ���
		ai1.test(new Device() {
			// ��ʼ����
			{
				System.out.println("�����ڲ���ĳ�ʼ����...");
			}
			@Override
			public double getPrice() {
				// TODO Auto-generated method stub
				return 0;
			}
			public String getName()
			{
				return "����";
			}
		});
		
		AnonymousInner ai2 = new AnonymousInner();
		// �����в����Ĺ���������Device����ʵ����Ķ���
		ai2.test(new Device("����ʾ����")
		{
			public double getPrice()
			{
				return 67.8;
			}
		});
	}
}

abstract class Device
{
	private String name;
	public abstract double getPrice();
	public Device(){}
	public Device(String name)
	{
		this.name = name;
	}
	// �˴�ʡ����name��setter��getter����
	public void setName(String name)
	{
		this.name = name;
	}
	public String getName()
	{
		return this.name;
	}
}