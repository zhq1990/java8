package com.java.java8.lambda.inner;

/*
 * https://blog.csdn.net/chengqiuming/article/details/91352913
 * 点睛
 * 匿名内部类适合创建那种只需要一次使用的类，定义匿名内部类的语法格式如下：
	new 父类构造器（实参列表) | 实现接口()
	{
	    //匿名内部类的类体部分
	}
	从上面的定义可以看出，匿名内部类必须继承一个父类，或实现一个接口，但最多只能继承一个父类，或实现一个接口。
	两条规则。
		匿名内部类不能是抽象类。
		匿名内部类不能定义构造器。由于匿名内部类没有类名，所以无法定义构造器，但匿名内部类可以初始化块，可以通过初始化块来完成构造器需要完成的工作。
————————————————
原文链接：https://blog.csdn.net/chengqiuming/article/details/91352913
 */

/*
 * 1 点睛
	最常用的创建匿名内部类的方式是创建某个接口类型的对象。
 */
public class AnonymousTest
{
	public void test(Product p)
	{
		System.out.println("购买了一个" + p.getName()
		+ "，花掉了" + p.getPrice());
	}
	public static void main(String[] args)
	{
		AnonymousTest ta = new AnonymousTest();
		// 调用test()方法时，需要传入一个Product参数，
		// 此处传入其匿名内部类的实例
		ta.test(new Product()
		{
			public double getPrice()
			{
				return 567.8;
			}
			public String getName()
			{
				return "AGP显卡";
			}
		});
	}
}


interface Product
{
	public double getPrice();
	public String getName();
}