package com.java.java8.lambda.inner;

/*
 * https://blog.csdn.net/LeoZuosj/article/details/82317931
 * �����ڲ���ĸ�ʽ�����
	A�������ڲ���
		�����ڲ���ļ�д����
	B��ǰ�᣺����һ������߽ӿ�
		�����������Ǿ�����Ҳ�����ǳ����ࡣ
		�����ڲ��࣬��˼����û�����֡�û�����־���Ҫ��취��ʾ����
		��ô��ʾ���أ�����Ҫʵ��һ���ӿڻ��߼̳�һ���ࡣ
		����Ҫ�������ĳ���ӿڻ���ĳ���������ϵ������������ڲ��ࡣ
	C����ʽ
		new �������߽ӿ���(){
			��д����;
		}
	D��������ʲô��?
		��һ���̳��˸������ʵ���˸ýӿڵ�������������
	E��������ʾ

��������������������������������
��Ȩ����������ΪCSDN���������ܿ�����ԭ�����£���ѭ CC 4.0 BY-SA ��ȨЭ�飬ת���븽��ԭ�ĳ������Ӽ���������
ԭ�����ӣ�https://blog.csdn.net/LeoZuosj/article/details/82317931
 */

class Demo04_NoNameInnerClass{
    public static void main(String[] args){
        Outer o = new Outer();
        o.method();
    }
}

interface Inter{
    public void print();
}

/*
//�����ֵ��ڲ���
class Outer{
    class Inner implements Inter{
        public void print() {
            System.out.println("print");
        }
    }

    public void method(){
        //Inner i = new Inner();
        //i.print();
        new Inner().print();
    }
}*/

//�����ڲ���
//�����ڲ����Ǿֲ��ڲ����һ�֣����Ա���д�ڷ����
class Outer{
    public void method(){
        //��������Inter���������,Ȼ�����print����
        new Inter(){   
        	@Override //ʵ��Inter�ӿ�
            public void print(){        //��д���󷽷�
                System.out.println("print��д���󷽷�");
            }
        }.print();
    }
}
