package com.java.恋上算法.数据结构._04_栈;

import com.java.恋上算法.数据结构._03_链表.ArrayList;
import com.java.恋上算法.数据结构._03_链表.List;

/**
 * @desc: xxx
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/9/4/0004 下午 08:29
 */
public class Stack<E> {

    private List<E> list = new ArrayList<>();


    public void clear() {
        list.clear();
    }

    public int size()
    {
        return list.size();
    }

    public boolean isEmpty() // 是否为空
    {
        return list.isEmpty();
    }

    public void push(E element) // 入栈
    {
        list.add(element);
    }

    public E pop() // 出栈
    {
        return list.remove(list.size() - 1);
    }

    public E top() // 获取栈顶元素
    {
        return list.get(list.size() - 1);
    }

}
