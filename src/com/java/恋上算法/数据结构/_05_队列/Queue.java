package com.java.恋上算法.数据结构._05_队列;

import com.java.恋上算法.数据结构._03_链表.LinkedList;
import com.java.恋上算法.数据结构._03_链表.List;

/**
 * @desc: 队列
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/9/6/0006 下午 07:26
 */
public class Queue<E> {
    private List<E> list = new LinkedList<>();

    public int size() {
        return list.size();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public void clear() {
        list.clear();
    }

    public void enQueue(E element) {
        list.add(element);
    }

    public E deQueue() {
        return list.remove(0);
    }

    public E front() {
        return list.get(0);
    }
}
