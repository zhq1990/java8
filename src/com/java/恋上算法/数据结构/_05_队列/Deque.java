package com.java.恋上算法.数据结构._05_队列;

import com.java.恋上算法.数据结构._03_链表.LinkedList;
import com.java.恋上算法.数据结构._03_链表.List;

/**
 * @desc: 双端队列
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/9/6/0006 下午 07:18
 */
public class Deque<E> {

    private List<E> list = new LinkedList<>();

    int size() // 元素的数量
    {
        return list.size();
    }

    boolean isEmpty() // 是否为空
    {
        return list.isEmpty();
    }

    void clear() // 清空
    {
        list.clear();
    }

    void enQueueRear(E element) // 从队尾入队
    {
        list.add(element);
    }

    E deQueueFront() // 从队头出队
    {
        return list.remove(0);
    }

    void enQueueFront(E element) // 从队头入队
    {
        list.add(0,element);
    }

    E deQueueRear() // 从队尾出队
    {
        return list.remove(list.size() - 1);
    }

    E front() // 获取队列的头元素
    {
        return list.get(0);
    }

    E rear() // 获取队列的尾元素
    {
        return list.get(list.size() - 1);
    }


}
