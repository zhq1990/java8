package com.java.恋上算法.数据结构._05_队列.circle;

/**
 * @desc: 循环队列
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/9/6/0006 下午 07:29
 */
public class CircleQueue<E> {
    private int front;
    private int size;
    private E[] elements;
    private static final int DEFAULT_CAPACITY = 10;

    public CircleQueue(){
        elements = (E[]) new Object[DEFAULT_CAPACITY];
    }
    /**
     * 元素的数量
     */
    public int size() {
        return size;
    }
    /**
     * 是否为空
     * @param
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     *  清空
     */
    public void clear() {
        for (int i = 0;i < size;i ++){
            elements[index(i)] = null;
        }
        front = 0;
        size = 0;
    }

    /**
     *  从队尾入队
     * @param element
     */
    public void enQueue(E element) {
        ensureCapacity(size + 1);

        elements[index(size)] = element;
        size ++;
    }

    /**
     *  从队头出队
     * @return
     */
    public E deQueue() {
        E frontElement = elements[front];
        elements[front] = null;
        front = index(1); // 1为偏移量
        size --;
        return frontElement;
    }

    /**
     *  获取队列的头元素
     * @return
     */
    public E front() {
        return elements[front];
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("capcacity=").append(elements.length)
                .append(" size=").append(size)
                .append(" front=").append(front)
                .append(", [");
        for (int i = 0; i < elements.length; i++) {
            if (i != 0) {
                string.append(", ");
            }

            string.append(elements[i]);
        }
        string.append("]");
        return string.toString();
    }

    /**
     * 索引封装
     * @param index
     * @return
     */
    private int index(int index) {
//        return (front +index) % elements.length;
        index += front;
        return index - (index >= elements.length ? elements.length : 0);
    }


    /**
     * 保证要有capacity的容量
     * @param capacity
     */
    private void ensureCapacity(int capacity) {
        int oldCapacity = elements.length;
        if (oldCapacity >= capacity) {
            return;
        }

        // 新容量为旧容量的1.5倍
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        E[] newElements = (E[]) new Object[newCapacity];
        for (int i = 0; i < size; i++) {
            newElements[i] = elements[index(i)];
        }
        elements = newElements;

        // 重置front
        front = 0;
    }
}
