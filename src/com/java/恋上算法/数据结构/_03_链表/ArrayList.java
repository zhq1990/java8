package com.java.恋上算法.数据结构._03_链表;

/**
 * @desc: 动态数组 抽象类抽取改造
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/8/8/0008 下午 06:01
 * @param <E> 代表元素的类型(element),类的后边定义泛型
 */
public class ArrayList<E>  extends AbstractList<E> {

    private static final int DEFAULT_CAPACITY = 10;
    /**
     * 所有的元素
     */
    private E[] elements;

    /**
     * 指定容量的构造函数
     * @param capaticy
     */
    public ArrayList(int capaticy){
        capaticy = (capaticy < DEFAULT_CAPACITY ) ? DEFAULT_CAPACITY :capaticy;
        //Object 是所有类的父类，强制类型转换
        elements = (E[]) new Object[capaticy];

    }

    /**
     * 默认无参构造函数
     */
    public ArrayList(){
        this(DEFAULT_CAPACITY);
    }

    /**
     * 清楚数组
     */
    @Override
    public void  clear(){
        for (int i = 0; i < size; i++) {
            elements[i] = null;
        }
        size = 0;
    }

    /**
     * 获取index位置的元素
     * @param index
     * @return
     */
    @Override
    public E get(int index) {
        rangeCheck(index);
        return elements[index];
    }

    /**
     * 设置index位置的元素
     * @param index
     * @param element
     * @return 原来的元素ֵ
     */
    @Override
    public E set(int index, E element) {
        rangeCheck(index);

        E old = elements[index];
        elements[index] = element;
        return old;

    }

    /**
     * 在index位置插入一个元素
     * @param index
     * @param element
     */
    @Override
    public void add(int index, E element) {
        rangeCheckForAdd(index);

        ensureCapacity(size + 1);

        for (int i=size;i>index;i--){
            elements[i] = elements[i -1];
        }
        elements[index] = element;
        size++;
    }

    /**
     * 删除index位置的元素
     * @param index
     * @return
     */
    @Override
    public E remove(int index) {
        rangeCheck(index);

        E old = elements[index];
        for (int i = index + 1; i < size; i++) {
            elements[i - 1] = elements[i];
        }
        //size --;
        //elements[size] = null;
        /*等价于*/
        elements[--size] = null;
        return old;
    }

    /**
     * 查看元素的索引
     * @param element
     * @return
     */
    @Override
    public int indexOf(E element) {
        if (element == null){
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0;i <size; i++){
                if (element.equals(elements[i])){
                    return i;
                }
            }
        }
        return ELEMENT_NOT_FOUND;
    }

    /**
     * 扩容
     * 保证要有capacity的容量
     * @param capacity
     */
    private void ensureCapacity(int capacity) {
        int oldCapacity = elements.length;
        if (oldCapacity >= capacity){
            return;
        }

        // 新容量为旧容量的1.5倍
        int newCapacity = oldCapacity + (oldCapacity >>1 );
        E[] newElements = (E[]) new  Object[newCapacity];
        for (int i = 0 ;i<size;i++){
            newElements[i]=elements[i];
        }
        elements = newElements;
        System.out.println(oldCapacity + "扩容为" + newCapacity);
    }

    @Override
    public String toString() {
        // size=3, [99, 88, 77]
        StringBuilder string = new StringBuilder();
        string.append("size=").append(size).append(", [");
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                string.append(", ");
            }

            string.append(elements[i]);

//			if (i != size - 1) {
//				string.append(", ");
//			}
        }
        string.append("]");
        return string.toString();
    }

}
