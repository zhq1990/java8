package com.java.恋上算法.数据结构._03_链表;

/**
 * @desc: 链表接口
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/8/21/0021 下午 10:01
 */
public interface List<E> {
    /**
     * 为了给外界访问
     * 也可放在AbstractList中，不过AbstractList外界不可见
     * 外界只会使用ArrayList、LinkedList、List 这三个
     */
    static final int ELEMENT_NOT_FOUND = -1;

    /**
     * 清除所有元素
     */
    void clear();

    /**
     * 元素的数量
     * @return
     */
    int size();

    /**
     * 是否为空
     * @return
     */
    boolean isEmpty();

    /**
     * 是否包含某个元素
     * @param element
     * @return
     */
    boolean contains(E element);

    /**
     * 添加元素到尾部
     * @param element
     */
    void add(E element);

    /**
     * 获取index位置的元素
     * @param index
     * @return
     */
    E get(int index);

    /**
     * 设置index位置的元素
     * @param index
     * @param element
     * @return 原来的元素ֵ
     */
    E set(int index, E element);

    /**
     * 在index位置插入一个元素
     * @param index
     * @param element
     */
    void add(int index, E element);

    /**
     * 删除index位置的元素
     * @param index
     * @return
     */
    E remove(int index);

    /**
     * 查看元素的索引
     * @param element
     * @return
     */
    int indexOf(E element);

}
