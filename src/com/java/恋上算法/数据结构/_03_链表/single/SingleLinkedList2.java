package com.java.恋上算法.数据结构._03_链表.single;

import com.java.恋上算法.数据结构._03_链表.AbstractList;

/**
 * @desc: 带有虚拟头结点的链表
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/8/21/0021 下午 08:55
 */
public class SingleLinkedList2<E>  extends AbstractList<E> {
    private Node<E> first ;

    public SingleLinkedList2() {
        first = new Node<>(null,null);
    }

    /**
     * 内部类节点
     * @param <E>
     */
    private static class Node<E>{
        E element;
        Node<E> next; // 下一个node的地址
        // 构造函数
        public Node(E element,Node<E> next){
            this.element = element;
            this.next = next;
        }
    }

    @Override
    public void clear() {
        size = 0;
        first = null;
    }

    @Override
    public E get(int index) {
        return node(index).element;
    }

    @Override
    public E set(int index, E element) {
        Node<E> node = node(index);
        E old = node.element;
        node.element =element;
        return old;
    }

    @Override
    public void add(int index, E element) {
        rangeCheckForAdd(index);
        // 获取前一个指针
        Node<E> previous = index == 0 ? first : node(index - 1);
        // 创建新的节点传参元素和next节点，然后将新建节点赋值给previous节点的next指针
        previous.next = new Node<>(element, previous.next);

        size ++;
    }

    @Override
    public E remove(int index) {
        rangeCheck(index);

        Node<E> prev = index ==0 ? first : node(index-1);
        Node<E> node = prev.next;
        prev.next = prev.next.next;

        size --;
        return node.element;
    }

    @Override
    public int indexOf(E element) {
        if (element == null) {
            // 遍历节点
            Node<E> node = first;
            for (int i = 0; i < size; i++) {
                if (node.element == null) {
                    return i;
                }
                node = node.next;
            }
        } else {
            Node<E> node = first;
            for (int i = 0; i < size; i++) {
                if (element.equals(node.element)) {
                    return i;
                }
                node = node.next;
            }
        }
        return ELEMENT_NOT_FOUND;
    }

    /**
     * 获取index位置对应的节点对象
     * @param index
     * @return
     */
    private Node<E> node(int index) {
        rangeCheck(index);

        Node<E> node = first.next;
        for (int i = 0; i < index; i++){
            node = node.next;
        }

        return node;
    }


    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("size=").append(size).append(", [");
        Node<E> node = first.next;
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                string.append(", ");
            }

            string.append(node.element);

            node = node.next;
        }
        string.append("]");
        return string.toString();
    }



}
