package com.java.恋上算法.数据结构._03_链表;

import com.java.恋上算法.数据结构._03_链表.circle.CircleLinkedList;
import com.java.恋上算法.数据结构._03_链表.circle.CircleLinkedListPlus;
import com.java.恋上算法.数据结构._03_链表.circle.SingleCircleLinkedList;
import com.java.恋上算法.数据结构.utils.Asserts;

/**
 * @desc: xxx
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/8/21/0021 下午 10:40
 */
public class Main {

    public static void main(String[] args) {
        new java.util.LinkedList<>();


//		testList(new ArrayList<>());
//        testList(new LinkedList<>());

//        testList(new SingleCircleLinkedList<>());

//        testList(new CircleLinkedList<>());

        josephus();

    }

    /*public static void main(String[] args) {

     new java.util.LinkedList<>();

        System.out.println("添加测试开始 》》》");
        List<Integer> list = new LinkedList<>();
        // List<Integer> list = new LinkedList2<>();
        list.add(20);
        list.add(0,10);
        list.add(30);
        list.add(list.size(),40);
        // [10,20,30.40]

        list.remove(1);
        System.out.println(list);
        System.out.println("添加测试结束 《《《");

        System.out.println("扩缩容测试开始 》》》");
        List<Integer> list1 = new ArrayList2<>();
        for (int i = 0; i < 50; i++){
            list1.add(i);
        }
        for (int i = 0 ; i< 50; i ++){
            list1.remove(0);
        }
        System.out.println(list1);
        System.out.println("扩缩容测试结束 《《《");
    }*/

    static void testList(List<Integer> list) {
        list.add(11);
        list.add(22);
        list.add(33);
        list.add(44);

        list.add(0, 55); // [55, 11, 22, 33, 44]
        list.add(2, 66); // [55, 11, 66, 22, 33, 44]
        list.add(list.size(), 77); // [55, 11, 66, 22, 33, 44, 77]

        list.remove(0); // [11, 66, 22, 33, 44, 77]
        list.remove(2); // [11, 66, 33, 44, 77]
        list.remove(list.size() - 1); // [11, 66, 33, 44]

        Asserts.test(list.indexOf(44) == 3);
        Asserts.test(list.indexOf(22) == List.ELEMENT_NOT_FOUND);
        Asserts.test(list.contains(33));
        Asserts.test(list.get(0) == 11);
        Asserts.test(list.get(1) == 66);
        Asserts.test(list.get(list.size() - 1) == 44);

        System.out.println(list);
    }

    static void josephus() {
        CircleLinkedListPlus<Integer> list = new CircleLinkedListPlus<>();
        for (int i = 1; i <= 8; i++) {
            list.add(i);
        }

        // current 指向头结点（指向1）
        list.reset();

        while (!list.isEmpty()) {
            // 数三个其实是跳两次，其实就是逢3的倍数删掉
            list.next();
            list.next();
            System.out.println(list.remove());
        }
    }



}
