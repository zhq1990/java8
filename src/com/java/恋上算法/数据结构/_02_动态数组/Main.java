package com.java.恋上算法.数据结构._02_动态数组;

/**
 * @desc: xxx
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/8/8/0008 下午 06:01
 */
public class Main {

    public static void main(String[] args) {
        new java.util.ArrayList<>();
        // int -> Integer

        // new是向堆空间申请内存
        // 类型只能用包装类型
        ArrayList<Integer> list = new ArrayList<>();
        //list.get(0);
        list.add(10);
        list.add(22);
        list.add(33);
        list.add(44);
        System.out.println(list);

		ArrayList<Object> persons  = new ArrayList<>();
		persons.add(new Person(10, "Jack"));
		persons.add(new Person(15, "Rose"));
		persons.add(new Person(12, "James"));
		System.out.println(persons);
		persons.clear();
		// 提醒JVm垃圾回收
        System.gc();
    }

}
