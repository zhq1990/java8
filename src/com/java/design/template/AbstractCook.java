package com.java.design.template;

/**
 * @desc: 模板模式
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/3/15 下午7:21
 */
public abstract class AbstractCook {

    public void steamedRice(){
        wash();
        putRice();
        putWater();
        steam();
    }

    protected abstract void wash();
    protected abstract void putWater();
    protected abstract void putRice();
    protected abstract void steam();

}
