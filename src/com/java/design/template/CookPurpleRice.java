package com.java.design.template;

/**
 * @desc:
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/3/15 下午7:26
 */
public class CookPurpleRice extends AbstractCook{
    @Override
    protected void wash() {
        System.out.println("洗一遍");
    }

    @Override
    protected void putWater() {
        System.out.println("防水");
    }

    @Override
    protected void putRice() {
        System.out.println("放米");
    }

    @Override
    protected void steam() {
        System.out.println("蒸45分钟");
    }

    public static void main(String[] args) {
        AbstractCook cook = new CookPurpleRice();
        cook.steamedRice();
    }
}
