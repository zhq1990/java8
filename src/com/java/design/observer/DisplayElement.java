package com.java.design.observer;

/**
 * @desc: xxx
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/3/22 下午7:31
 */
public interface DisplayElement {
    public void display();		//观察者调用此方法向用户显示数据
}
