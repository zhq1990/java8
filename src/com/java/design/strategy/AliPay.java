package com.java.design.strategy;

/**
 * @desc: xxx
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/3/8 下午8:38
 */
public class AliPay implements PayStrategy {

    @Override
    public void pay(Order ctx) {
        System.out.println("欢迎使用支付宝");
        System.out.println("本地交易金额为"+ctx.getMoney()+"，开始扣款");
    }
}
