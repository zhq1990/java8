package com.java.design.strategy;

/**
 * @desc: xxx
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/8/8/0008 下午 10:11
 */
public interface PayStrategy {

    public void pay(Order ctx);

}
