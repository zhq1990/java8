package com.java.design.strategy;

//支付环境类
//包含一个支付方式的引用
//以及其子类共有的数据元素
public class Order {
	private String userName;
	private String code;
	private int money;

	private PayStrategy payStrategy=null;

	public Order(String userName,String code, int money, PayStrategy payStrategy) {
		this.userName = userName;
		this.code =code;
		this.money = money;
		this.payStrategy = payStrategy;
	}
	public String getUserName() {
		return userName;
	}
	public int getMoney() {
		return money;
	}
	public void payNow(){
		this.payStrategy.pay(this);
	}
}
 
//支付策略接口
//定义支付策略的共有方法