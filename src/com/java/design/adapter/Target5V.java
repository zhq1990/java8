package com.java.design.adapter;

/**
 * @desc: 目标：5V
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/3/22 下午8:41
 */
public interface Target5V {
    void  chong5V();
}
