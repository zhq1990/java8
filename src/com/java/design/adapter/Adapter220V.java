package com.java.design.adapter;

/**
 * @desc: 被适配电压：220V
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/3/22 下午8:41
 */
public class Adapter220V {
    public void chong220V(){
        System.out.println("充220V电啦");
    }
}
