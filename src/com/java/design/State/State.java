package com.java.design.State;

//状态
enum State
{
    SMALL, //普通状态
    SUPER,  //超级状态
    FIRE,   //火焰状态
    DEAD    //死亡状态
};