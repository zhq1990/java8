package com.java.design.link;

/**
 * @desc: xxx
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/3/22 下午8:06
 */
public class Tencent extends Server {
    @Override
    public void buyServer(Cpu cpu,Memary memary) {
        System.out.println("购买腾讯云");
        cpu.addCpu();
        memary.addMemary();
    }
}
