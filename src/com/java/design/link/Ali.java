package com.java.design.link;

/**
 * @desc: xxx
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/3/22 下午7:59
 */
public class Ali extends Server{
    @Override
    public void buyServer(Cpu cpu,Memary memary) {
        System.out.println("购买阿里云");
        cpu.addCpu();
        memary.addMemary();

    }
}
