package com.java.design.link;

/**
 * @desc: 桥接模式
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/3/22 下午8:01
 */
public class Test {
    public static void main(String[] args) {
        Ali ali =new Ali();
        /*ali.setCpu(new Cpu4());
        ali.setMemary(new Memary16G());*/
        ali.buyServer(new Cpu4(),new Memary16G());

        Tencent tencent =new Tencent();
        /*tencent.setCpu(new Cpu4());
        tencent.setMemary(new Memary16G());
        tencent.buyServer();*/
    }
}
