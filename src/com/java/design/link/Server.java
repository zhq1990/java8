package com.java.design.link;

/**
 * @desc: xxx
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/8/8/0008 下午 10:13
 */
public abstract class Server {

    public abstract void buyServer(Cpu cpu,Memary memary);

}
