package com.java.design.facade;

/**
 * @desc: xxx
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/3/29 下午7:35
 */
class Cal {
    public void cal(String type, int a , int b){
        Add add=new Add();
        Subtract subtract=new Subtract();
        Multiply multiply=new Multiply();
        Divide divide=new Divide();

        switch (type){
            case "add":
                add.add(a,b);
                break;
            case "subtract":
                add.add(a,b);
                break;
            case "multiply":
                add.add(a,b);
                break;
            case "divide":
                add.add(a,b);
                break;
        }
    }
}

class Add {
    public int add(int a,int b){
        return a + b;
    }

}

class Subtract{
    public int subtract(int a,int b){
        return a - b;
    }
}

 class Multiply{
     public int multiply(int a,int b){
         return a * b;
     }

}

 class Divide{

     public int divide(int a,int b){
         return a / b;
     }
}
