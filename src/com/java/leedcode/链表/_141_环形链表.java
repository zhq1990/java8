package com.java.leedcode.链表;

/**
 * @desc: https://leetcode-cn.com/problems/linked-list-cycle/
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/8/24/0024 下午 09:57
 */
public class _141_环形链表 {
     public boolean hasCycle(ListNode head){
         // 快慢指针思想：追及问题
         //快慢指针，快指针一次走两步，慢指针一次走一步
         //如果有环，两个指针一定会在某点相遇
         if (head ==null || head.next ==null){
             return false;
         }
         //两个指针初始不能是同一个位置
         ListNode slow = head;
         ListNode fast = head.next;
         while(fast !=null && fast.next!=null) {
             slow = slow.next;
             fast = fast.next.next;
             if (slow == fast){
                 return true;
             }
         }
         return false;
     }
}
