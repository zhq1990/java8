package com.java.leedcode.链表;

/**
 * @desc: xxx
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/8/22/0022 下午 10:42
 */
public class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) {
        val = val;
    }
    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next; }
}
