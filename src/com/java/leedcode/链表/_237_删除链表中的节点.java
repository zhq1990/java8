package com.java.leedcode.链表;

/**
 * @desc: https://leetcode-cn.com/problems/delete-node-in-a-linked-list/
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/8/22/0022 下午 10:23
 */
public class _237_删除链表中的节点 {

    public void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;
    }

}
