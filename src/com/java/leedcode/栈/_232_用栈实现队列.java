package com.java.leedcode.栈;

import java.util.Stack;

/**
 * @desc: https://leetcode-cn.com/problems/implement-queue-using-stacks/
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/9/6/0006 下午 08:59
 */
public class _232_用栈实现队列 {

    private Stack<Integer> inStack;
    private Stack<Integer> outStack;

    public  _232_用栈实现队列(){
        inStack = new Stack<>();
        outStack = new Stack<>();
    }

    /** 入队 */
    public void push(int x) {
        inStack.push(x);
    }

    /** 出队 */
    public int pop() {
        checkOutStack();

        return outStack.pop();
    }


    /** 获取队头元素 */
    public int peek() {
        checkOutStack();

        return outStack.peek();
    }

    /** 是否为空 */
    public boolean empty() {
        return inStack.isEmpty() && outStack.isEmpty();
    }

    /**
     * 判断outStack是否为空，为空就将inStack循环放入outStack中
     */
    private void checkOutStack(){
        if (outStack.isEmpty()){
            while (!inStack.isEmpty()){
                outStack.push(inStack.pop());
            }
        }
    }

}
