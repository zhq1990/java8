package com.java.leedcode.demo2;

/**
 * @desc: xxx
 * @author: zhanghongqiang01@baijiahulian.com
 * @date: 2021/10/27/0027 下午 08:49
 */
public class Solution {

    public String Solution(String string) {
        if (string.length()<2){
            return string;
        }
        char [] str = string.toCharArray();
        int maxi = 0;
        int maxj = 0;
        for (int i = 0; i < str.length; i++) {
            int x = 0;
            int y = 0;
            for (int j = str.length-1; j >i; j--) {
                if (str[i]==str[j]){
                    int a = i+1;
                    int b= j-1;
                    boolean flag=true;
                    while (b>a){
                        if (str[b]==str[a]){
                            a++;
                            b--;
                        }
                        else {
                            flag = false;
                            break;
                        }
                    }
                    if (flag){
                        x = i;
                        y = j;
                    }
                }
                if (y-x>maxj-maxi){
                    maxi = x;
                    maxj = y;
                }
            }
        }

        return string.substring(maxi, maxj+1);
    }
}

